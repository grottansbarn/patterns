#pragma once

class Observer {
public:
    //transfering new data from Subject to Observers
    virtual void notify(int value) = 0;
    virtual ~Observer() = default;
};
