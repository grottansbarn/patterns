#pragma once

#include <vector>

#include "./observer.h"

class Subject {
private:
    int m_value;
    std::vector<Observer*> m_views;

public:
    //attach new Observer to this Subject
    void attach(Observer* obs)
    {
        //all of this Observers objects will be notified
        m_views.push_back(obs);
    }

    //setting the new data that need to be transfer to Observers objects
    void change(int value)
    {
        m_value = value;
        //notify all Observers about new data
        notify();
    }

    //transfering new data from Subject to Observers
    void notify()
    {
        for (int i = 0; i < m_views.size(); ++i)
            m_views[i]->notify(m_value);
    }
};
