#pragma once

#include <iostream>

#include "./observer.h"
#include "./subject.h"

class DivObserver : public Observer {
    int m_div;

public:
    DivObserver(Subject* model, int div)
    {
        //send address of this object to observed object
        model->attach(this);
        m_div = div;
    }

    //handling received data
    void notify(int v) /* virtual */
    {
        std::cout << v << " div " << m_div << " is " << v / m_div << '\n';
    }
};
