#pragma once

#include <iostream>

#include "./observer.h"
#include "./subject.h"

class ModObserver : public Observer {
    int m_mod;

public:
    ModObserver(Subject* model, int mod)
    {
        //connect this object to Subject events
        model->attach(this);
        m_mod = mod;
    }

    //handling received data
    void notify(int v) /* virtual */
    {
        std::cout << v << " mod " << m_mod << " is " << v % m_mod << '\n';
    }
};
