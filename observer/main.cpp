#include "./div_observer.h"
#include "./mod_observer.h"
#include "./subject.h"

int main()
{
    Subject subj;

    DivObserver divObs1(&subj, 4);
    DivObserver divObs2(&subj, 3);

    ModObserver modObs3(&subj, 3);

    subj.change(14);

    return EXIT_SUCCESS;
}
