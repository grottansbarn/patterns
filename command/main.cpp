#include <iostream>

#include "command.hpp"
#include "invoker.hpp"
#include "receiver.hpp"

/* client code can parametrize Invoker by any Commands */
int main() {

    Invoker* invoker = new Invoker;
    Receiver* receiver = new Receiver;

    invoker->SetOnStart(new SimpleCommand("Say Hi!"));
    invoker->SetOnFinish(new ComplexCommand(receiver, "Send email", "Save report"));
    invoker->DoSomethingImportant();

    delete invoker;
    delete receiver;

    return 0;
}
