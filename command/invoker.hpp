#pragma once

#include <iostream>

#include "command.hpp"

/* Invoker linked with some Commands. It can transmit the request by the Command */
class Invoker {
public:
    ~Invoker() {
        delete on_start_;
        delete on_finish_;
    }

    void SetOnStart(Command *command) {
        on_start_ = command;
    }
    void SetOnFinish(Command *command) {
        on_finish_ = command;
    }

    /* Отправитель не зависит от классов конкретных команд и получателей.
     * Отправитель передаёт запрос получателю косвенно, выполняя команду.*/
    void DoSomethingImportant() {
        std::cout << "Invoker: Does anybody want something done before I begin?\n";
        if (on_start_) {
            on_start_->Execute();
        }
        std::cout << "Invoker: ...doing something really important...\n";
        std::cout << "Invoker: Does anybody want something done after I finish?\n";
        if (on_finish_) {
            on_finish_->Execute();
        }
    }
private:
    Command* on_start_;
    Command* on_finish_;
};
