#pragma once

#include <iostream>

#include "receiver.hpp"

/* interface */
class Command {
public:
    virtual ~Command() {}
    virtual void Execute() const = 0;
};


/* some Comands can do simple operations by itself */
class SimpleCommand : public Command {
public:
    explicit SimpleCommand(std::string pay_load) : pay_load_(pay_load) {}
    void Execute() const override {
        std::cout << "SimpleCommand: See, I can do simple things like printing (" << pay_load_ << ")\n";
    }
private:
    std::string pay_load_;
};


/* some Commands delegate difficult operations to another objects - receivers */
class ComplexCommand : public Command {
public:
    ComplexCommand(Receiver *receiver, std::string a, std::string b) : receiver_(receiver), a_(a), b_(b) {}

    /* Команды могут делегировать выполнение любым методам получателя.*/
    void Execute() const override {
        std::cout << "ComplexCommand: Complex stuff should be done by a receiver object.\n";
        receiver_->DoSomething(a_);
        receiver_->DoSomethingElse(b_);
    }
private:
    Receiver* receiver_;
    /* context */
    std::string a_;
    std::string b_;
};
