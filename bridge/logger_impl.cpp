// LoggerImpl.cpp - implementation

#include <iostream>

#include "logger_impl.h"

void ST_logger_impl::console_log(const std::string& str)
{
    std::cout << "Single-threaded console logger" << std::endl;
}

void ST_logger_impl::file_log(std::string& file, const std::string& str)
{
    std::cout << "Single-threaded file logger" << std::endl;
}

void ST_logger_impl::socket_log(std::string& host, int port, const std::string& str)
{
    std::cout << "Single-threaded socket logger" << std::endl;
};

void MT_logger_impl::console_log(const std::string& str)
{
    std::cout << "Multithreaded console logger" << std::endl;
}

void MT_logger_impl::file_log(std::string& file, const std::string& str)
{
    std::cout << "Multithreaded file logger" << std::endl;
}

void MT_logger_impl::socket_log(std::string& host, int port, const std::string& str)
{
    std::cout << "Multithreaded socket logger" << std::endl;
}
