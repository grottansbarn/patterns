#include <string>

#include "./logger.h"

int main()
{
    logger* p = new file_logger(std::string("log.txt"));

    p->log(std::string("message"));

    delete p;

    return EXIT_SUCCESS;
}
