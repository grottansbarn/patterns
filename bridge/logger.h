// logger.h - abstraction
#pragma once

#include <string>

#include "./logger_impl.h"

//leading declaration
class logger {
public:
    logger(logger_impl* p);
    virtual ~logger();
    virtual void log(const std::string& str) = 0;

protected:
    logger_impl* pimpl;
};

///////////////////////////////////////////////////////////////////////////////
class console_logger : public logger {
public:
    console_logger();
    void log(const std::string& str);
};

///////////////////////////////////////////////////////////////////////////////
class file_logger : public logger {
public:
    file_logger(const std::string& file_name);
    void log(const std::string& str);

private:
    std::string file;
};

///////////////////////////////////////////////////////////////////////////////
class socket_logger : public logger {
public:
    socket_logger(std::string& remote_host, int remote_port);
    void log(const std::string& str);

private:
    std::string host;
    int port;
};
