#pragma once

#include "state.h"
#include "state_machine.h"

class state_red : public state {
public:
    state_red(state_machine* context);

    void handle();

private:
    state_machine* current_state_machine;
};
