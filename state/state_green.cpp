#include <iostream>

#include "state_green.h"
#include "state_machine.h"
#include "state_yellow.h"

state_green::state_green(state_machine* context)
    : current_state_machine(context){};

void state_green::handle()
{
    std::cout << "Green\n";

    //in this place we can request the  transition to another state
    //if(condition)
    //  _context->setState(new State_yellow(_context));
    //else
    //  _context->setState(new State_red(_context));
    //...

    current_state_machine->set_state(new state_yellow(current_state_machine));
}
