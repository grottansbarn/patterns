#include <iostream>

#include "state_machine.h"
#include "state_red.h"
#include "state_yellow.h"

state_yellow::state_yellow(state_machine* context)
    : current_state_machine(context){};

void state_yellow::handle()
{
    std::cout << "Yellow\n";

    //in this place we can request the  transition to another state
    //if(condition)
    //  _context->  blue
    //else
    //  _context-> red
    //...

    current_state_machine->set_state(new state_red(current_state_machine));
}
