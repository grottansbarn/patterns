#pragma once

#include "state.h"

class state_machine {
public:
    state_machine();

    void set_state(state* s);

    void handle();

private:
    state* current_state;
};
