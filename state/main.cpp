#include <ctime>
#include <iostream>

#include "state_machine.h"

void sleep(clock_t wait)
{
    clock_t goal;
    goal = wait + clock();
    while (goal > clock())
        ;
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
    state_machine state_machine;

    int i = 0;
    int loop = 6;

    while (loop) {
        if (i % 3 == 0)
            std::cout << "\n---------\n"
                         "Session "
                      << ((i + 1) / 3) + 1
                      << "\n---------\n";

        state_machine.handle();

        i++;
        loop--;
    }

    return 0;
}
