#include "state_machine.h"
#include "state_red.h"

state_machine::state_machine()
{
    // set first state of machine
    current_state = new state_red(this);
}

void state_machine::set_state(state* state)
{
    delete current_state;

    current_state = state;
}

void state_machine::handle()
{
    current_state->handle();
}
