#pragma once

class state {
public:
    virtual void handle() = 0;

    virtual ~state() = default;
};
