#pragma once

#include "state.h"
#include "state_machine.h"

class state_green : public state {
public:
    state_green(state_machine* context);

    void handle();

private:
    state_machine* current_state_machine;
};
