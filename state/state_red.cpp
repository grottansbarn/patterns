#include <iostream>

#include "state_green.h"
#include "state_machine.h"
#include "state_red.h"

state_red::state_red(state_machine* context)
    : current_state_machine(context){};

void state_red::handle()
{
    std::cout << "Red\n";

    //in this place we can request the  transition to another state
    //if(condition)
    //  _context->  blue
    //else
    //  _context-> red
    //...

    current_state_machine->set_state(new state_green(current_state_machine));
}
